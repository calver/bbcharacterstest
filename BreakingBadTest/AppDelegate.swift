//
//  AppDelegate.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var app: AppCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        let appCoordinator = AppCoordinator(window: window)
        app = appCoordinator
        
        appCoordinator.start()
        
        return true
    }
}
