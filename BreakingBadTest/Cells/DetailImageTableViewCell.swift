//
//  DetailImageTableViewCell.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

class DetailImageTableViewCell: UITableViewCell {

    static let reuseIdentifier = "DetailImageTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let characterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    func config() {
        selectionStyle = .none
        accessoryType = .none
        
        addSubview(characterImageView)
        
        characterImageView.translatesAutoresizingMaskIntoConstraints = false
                
        NSLayoutConstraint.activate([
            characterImageView.topAnchor.constraint(equalTo: topAnchor),
            characterImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            characterImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    var imageUrl: String! {
        didSet {
            characterImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"), options: .highPriority) { (image, error, cacheType, url) in
                
                guard let image = image else { return }
                self.characterImageView.image = image.resizeToSize(CGSize(width: 200,height: 200))
            }
        }
    }
}
