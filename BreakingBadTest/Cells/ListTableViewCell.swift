//
//  ListTableViewCell.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit
import SDWebImage

class ListTableViewCell: UITableViewCell {

    static let reuseIdentifier = "ListTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var outerStackView: UIStackView!
    var innerStackView: UIStackView!
    
    let characterImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    let characterName: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    let characterApperances: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    func config() {
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        
        characterImageView.translatesAutoresizingMaskIntoConstraints = false
        characterImageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)

        NSLayoutConstraint.activate([
            characterImageView.widthAnchor.constraint(equalToConstant: 40),
            characterImageView.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        characterName.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        innerStackView = UIStackView(arrangedSubviews: [characterName, characterApperances])
        innerStackView.distribution = .fill
        innerStackView.axis = .vertical
        innerStackView.spacing = 10
        
        outerStackView = UIStackView(arrangedSubviews: [characterImageView, innerStackView])
        outerStackView.alignment = .leading
        outerStackView.distribution = .fill
        outerStackView.axis = .horizontal
        outerStackView.spacing = 10
        
        addSubview(outerStackView)
        outerStackView.translatesAutoresizingMaskIntoConstraints = false
                
        NSLayoutConstraint.activate([
            outerStackView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            outerStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            outerStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            outerStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 10),
        ])
    }
    
    var character: BBCharacter! {
        didSet {
            characterImageView.sd_setImage(with: URL(string: character.img), placeholderImage: UIImage(named: "placeholder"), options: .highPriority) { (image, error, cacheType, url) in
                
                guard let image = image else { return }
                self.characterImageView.image = image.resizeToSize(CGSize(width: 40,height: 40))
            }
            
            characterName.text = character.name
            characterApperances.text = character.displayableAppearances
        }
    }
}
