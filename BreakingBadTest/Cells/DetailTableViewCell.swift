//
//  DetailTableViewCell.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    static let reuseIdentifier = "DetailTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var stackView: UIStackView!
    
    let detail: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    func config() {
        selectionStyle = .none
        accessoryType = .none
                
        addSubview(detail)
        detail.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            detail.centerYAnchor.constraint(equalTo: centerYAnchor),
            detail.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            detail.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
        ])
    }
    
    var characterDetail: String! {
        didSet {
            detail.text = characterDetail
        }
    }
}
