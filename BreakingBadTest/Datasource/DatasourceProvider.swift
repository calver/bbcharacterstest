//
//  DatasourceProvider.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

protocol DataSourceProviderDelegate {
    func editingState(_ editing: Bool)
}

protocol DataSourceProvider {
    associatedtype DataManagerType
    
    var dataManager: DataManagerType { get set }
    func fetchData()
}
