//
//  ListDatasourceProviderMock.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

@testable import BreakingBadTest

struct ListDatasourceProviderMock: ListDataSourceProviderType {
    
    typealias DataManagerType = ListDataManagerMock
    var dataManager: DataManagerType
    
    func fetchData() {
        
    }
    
    
    func performSearch(searchQuery: String?, seasonFilter: BBSeason) {
        
    }
    
}
