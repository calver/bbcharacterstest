//
//  ListDatasourceProvider.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit
import Combine

enum ListSection: String, CaseIterable {
    case allCharacters = "All characters"
    
    static var allSections: [ListSection] {
        return [.allCharacters]
    }
}

protocol ListDataSourceProviderType: DataSourceProvider {
    func performSearch(searchQuery: String?, seasonFilter: BBSeason)
}

extension ListDataSourceProviderType {
    static func performSearch(onCharacters characters: [BBCharacter], searchQuery: String?, seasonFilter: BBSeason) -> [BBCharacter] {
        
        let result: [BBCharacter]
        let searchedCharacters: [BBCharacter]
        
        if let searchQuery = searchQuery, !searchQuery.isEmpty {
            searchedCharacters = characters.filter { $0.contains(query: searchQuery) }
        } else {
            searchedCharacters = characters
        }
        
        if seasonFilter == .all {
            result = searchedCharacters
        } else {
            result = searchedCharacters.filter { $0.appearedInSeason(query: seasonFilter.rawValue) }
        }
        
        return result
    }
}

protocol ListDataSourceProviderDelegate {
    func characterSelected(_ character: BBCharacter)
}

public class ListDataSourceProvider: NSObject, ListDataSourceProviderType {
    
    typealias CellType = ListTableViewCell
    typealias DataManagerType = ListDataManager
    
    private var cancellables: Set<AnyCancellable> = []
    var delegate: ListDataSourceProviderDelegate?
    var dataManager: DataManagerType
    
    init(dataManager: DataManagerType, tableView: UITableView, delegate: ListDataSourceProviderDelegate?) {
        self.dataManager = dataManager
        self.tableView = tableView
        self.delegate = delegate
        
        super.init()
        
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        bindDataManager()
    }
    
    var tableView: UITableView!
    
    func requestData(forItemid id: String) {
        dataManager.fetchData()
    }
    
    func bindDataManager() {
        dataManager.$items.sink { [weak self] items in
            DispatchQueue.main.async {
                self?.applySnapshot(items: items, animatingDifferences: true)
            }
            
        }.store(in: &cancellables)
    }
    
    func fetchData() {
        dataManager.fetchData()
    }
    
    private var sections = ListSection.allSections
    lazy var dataSource = makeDataSource()
    typealias DataSource = UITableViewDiffableDataSource<ListSection, BBCharacter>
    typealias Snapshot = NSDiffableDataSourceSnapshot<ListSection, BBCharacter>
    
    func makeDataSource() -> DataSource {
        let dataSource = DataSource(
            tableView: tableView,
            cellProvider: { (tableView, indexPath, character) ->
                UITableViewCell? in
                
                guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: ListTableViewCell.reuseIdentifier,
                    for: indexPath) as? ListTableViewCell else { return nil }
                
                self.configureCell(cell, character: character)
                return cell
        })
        
        dataSource.defaultRowAnimation = .fade
        
        return dataSource
    }
}

extension ListDataSourceProvider: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let item = dataSource.itemIdentifier(for: indexPath) else { return }
        delegate?.characterSelected(item)
    }
}

extension ListDataSourceProvider {
    
    func configureCell(_ cell: ListTableViewCell, character: BBCharacter) {
        cell.character = character
    }
}

//MARK: diffable
extension ListDataSourceProvider {
    
    func applySnapshot(items: [BBCharacter], animatingDifferences: Bool = true) {
        var snapshot = Snapshot()
        snapshot.appendSections(sections)
        sections.forEach { section in
            snapshot.appendItems(items, toSection: section)
        }
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
}

//MARK: search filtering

extension ListDataSourceProvider {
    func performSearch(searchQuery: String?, seasonFilter: BBSeason) {
        let result = ListDataSourceProvider.performSearch(onCharacters: dataManager.items, searchQuery: searchQuery, seasonFilter: seasonFilter)
        
        DispatchQueue.main.async {
            self.applySnapshot(items: result)
        }
    }
}
