//
//  DetailDatasourceProvider.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

protocol CharacterDetailType {}

typealias ImageURLDataType = String
typealias StringDataType = String

enum DetailSection {
    case image(ImageURLDataType)
    case occupation(StringDataType)
    case status(StringDataType)
    case nickname(StringDataType)
    case appearances(StringDataType)
    
    static var allSections: [DetailSection] {
        return [.image(""), .occupation(""), .status(""), .nickname(""), .appearances("")]
    }
    
    var asSectionTitle : String {
        switch self {
        case .image(_):
            return ""
        case .occupation(_):
            return "occupation"
        case .status(_):
            return "status"
        case .nickname(_):
            return "nickname"
        case .appearances(_):
            return "appearances"
        }
    }
}

public class DetailDatasourceProvider: NSObject, UITableViewDataSource {
    
    let selectedCharacter: BBCharacter!
    private var dataSource: [DetailSection]? {
        didSet {
            guard let _ = dataSource else { return }
            tableView.reloadData()
        }
    }
    
    init(selectedCharacter: BBCharacter, tableView: UITableView) {
        self.selectedCharacter = selectedCharacter
        self.tableView = tableView

        super.init()
        
        tableView.dataSource = self
    }
    
    var tableView: UITableView!
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return DetailSection.allSections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let dataSource = dataSource else { return nil }
        guard let section = dataSource[safeIndex: section] else { return nil }
        
        
        return section.asSectionTitle
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dataSource = dataSource else { return UITableViewCell() }
        
        guard let section = dataSource[safeIndex: indexPath.section] else { return UITableViewCell() }
        
        switch section {
        case .image(let url):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailImageTableViewCell.reuseIdentifier) as? DetailImageTableViewCell else { return UITableViewCell() }
            cell.imageUrl = url
            return cell
        case .occupation(let detail), .status(let detail), .nickname(let detail), .appearances(let detail):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailTableViewCell.reuseIdentifier) as? DetailTableViewCell else { return UITableViewCell() }
            cell.characterDetail = detail
            return cell
        }
    }
}

extension DetailDatasourceProvider {
    func populateDatasource() {
        
        let url = selectedCharacter.img
        let occupation = selectedCharacter.displayableOccupations
        let status = selectedCharacter.status
        let nickname = selectedCharacter.nickname
        let appearances = selectedCharacter.displayableAppearances
        
        dataSource = [.image(url), .occupation(occupation), .status(status), .nickname(nickname), .appearances(appearances)]
    }
}
