//
//  URLSession+Additions.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

typealias NetworkResponse = (Data?, URLResponse?, Error?) -> Void

extension URLSession: NetworkSession {
    
    func loadData(from url: URL, completionHandler: @escaping NetworkResponse) {
        let task = dataTask(with: url) { (data, response, error) in
            completionHandler(data, response, error)
        }
        
        task.resume()
    }
    
    func postData(withRequest urlRequest: URLRequest, completionHandler: @escaping NetworkResponse) {
        let task = dataTask(with: urlRequest) { (data, response, error) in
            completionHandler(data, response, error)
        }
        
        task.resume()
    }
}
