//
//  UIStoryboard+Additions.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

extension UIStoryboard {
    // MARK: - Storyboards
    
    private static var home: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    // MARK: - View Controllers
    
    // Main
    static func instantiateListViewController() -> ListViewController {
        let controller = home.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
        return controller
    }
    
    static func instantiateDetailViewController() -> DetailViewController {
        let controller = home.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        return controller
    }
}
