//
//  UIImage+Additions.swift
//  BreakingBadTest
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

extension UIImage {
    func resizeToSize(_ size: CGSize) -> UIImage {
        
        // If you want "aspect fill", instead of "aspect fit", change function min to max.
        var scaledImageRect = CGRect.zero;
        
        let aspectWidth:CGFloat = size.width / self.size.width;
        let aspectHeight:CGFloat = size.height / self.size.height;
        let aspectRatio:CGFloat = max(aspectWidth, aspectHeight);
        
        scaledImageRect.size.width = self.size.width * aspectRatio;
        scaledImageRect.size.height = self.size.height * aspectRatio;
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        
        self.draw(in: scaledImageRect);
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return scaledImage!
    }
}
