//
//  Model.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

struct BBCharacter: Codable, Hashable {
    let char_id: Int
    let img: String
    let name: String
    let occupation: [String]
    let status: String
    let nickname: String
    let appearance: [Int]
    
    var displayableOccupations: String {
        let strings = occupation.map({"\($0)"})
        return strings.joined(separator:",")
    }
    
    var displayableAppearances: String {
        let strings = appearance.map({"\($0)"})
        return strings.joined(separator:",")
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(char_id)
    }
    
    func contains(query: String?) -> Bool {
        guard let query = query else { return true }
        guard !query.isEmpty else { return true }
        
        let queryLowerCased = query.lowercased()
        return name.lowercased().contains(queryLowerCased)
    }
    
    func appearedInSeason(query: Int?) -> Bool {
        guard let query = query else { return true }
        
        if query == 0 { return true }
        
        return appearance.contains(query)
    }
}
