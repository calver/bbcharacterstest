//
//  Constants.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

enum DeviceConfiguration {
    static let screenSize: CGRect = UIScreen.main.bounds
    static let screenWidth = screenSize.width
    static let screenHeight = screenSize.height
    static let adsEnabled = true
}

enum Strings {
    struct Titles {
        static let ListViewControllerTitle = "BB Characters"
    }
    struct Network {
        static let charactersEndpointUrl = "https://breakingbadapi.com/api/characters"
    }
    struct Misc {
        static let searchCharactersPlaceholder = "search characters"
    }
}
