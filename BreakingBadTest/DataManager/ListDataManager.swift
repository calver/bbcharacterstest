//
//  ListDataManager.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation
import Combine

protocol ListDataManagerType {}

class ListDataManager: DataManager, ListDataManagerType, ObservableObject {
    
    var apiService: BreakingBadAPIService!
    @Published var items = [BBCharacter]()
    
    required init(){}
    required convenience init(apiService: BreakingBadAPIService) {
        self.init()
        self.apiService = apiService
    }
}

extension ListDataManager {
    
    func fetchData() {
        let handler: FetchCharactersCompletionHandler = { [weak self] result in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let characters):
                strongSelf.items = characters
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        apiService.fetchCharacters(completionHandler: handler)
    }
}
