//
//  DataManager.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

protocol DataManager {
    associatedtype DataType
    associatedtype APIServiceType: APIService
    
    var apiService: APIServiceType! { get set }
    var items: [DataType] { get set }
    
    init(apiService: APIServiceType)
    func fetchData()
}

extension DataManager {
    var itemsCount: Int {
        return items.count
    }
    
    func item(at index: Int) -> DataType? {
        return items[safeIndex: index]
    }
}
