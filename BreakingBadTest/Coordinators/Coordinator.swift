//
//  Coordinator.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController { get set }
    func start()
}

extension Coordinator {
    func dismiss(animated: Bool) {
        navigationController.dismiss(animated: animated, completion: nil)
    }
    
    func dismissToRoot(animated: Bool) {
        navigationController.dismiss(animated: animated, completion: nil)
        popToRoot(animated: animated)
    }
    
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        navigationController.popToRootViewController(animated: animated)
    }
}
