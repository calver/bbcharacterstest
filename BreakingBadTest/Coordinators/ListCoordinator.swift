//
//  HomeCoordinator.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

class ListCoordinator: Coordinator {
    
    var appCoordinator: AppCoordinator
    var navigationController: UINavigationController
    private var childCoordinators: [Coordinator] = []
    
    init(navController: UINavigationController, appCoordinator: AppCoordinator) {
        self.appCoordinator = appCoordinator
        self.navigationController = navController
        
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.backgroundColor = .white
        
        if #available(iOS 11.0, *) {
            self.navigationController.navigationBar.prefersLargeTitles = true
        }
    }
    
    func start() {
        childCoordinators = []
        
        let controller = makeListViewController()
        navigationController.setViewControllers([controller], animated: true)
    }
        
    private func makeListViewController() -> ListViewController {
        let controller = UIStoryboard.instantiateListViewController()
        controller.title = "List"
        
        let networkManager = NetworkManager()
        let apiService = BreakingBadAPIService(networkManager: networkManager)
        let dataManager: ListDataManager = ListDataManager(apiService: apiService)
        
        controller.coordinator = self
        controller.dataManager = dataManager
        
        return controller
    }
    
    private func makeDetailViewController() -> DetailViewController {
        let controller = UIStoryboard.instantiateDetailViewController()
        controller.title = "Detail"
        controller.coordinator = self
        return controller
    }
}

extension ListCoordinator {
    
    func pushCharacterDetail(_ character: BBCharacter) {
        let controller = makeDetailViewController()
        controller.coordinator = self
        controller.selectedCharacter = character
        navigationController.pushViewController(controller, animated: true)
    }
}
