//
//  AppCoordinator.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

final class AppCoordinator {
    
    private let window: UIWindow
    private var childCoordinators: [Coordinator] = []

    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        showHome()
    }

    private func showHome() {
        childCoordinators = []
        
        let listCoordinator = ListCoordinator(navController: UINavigationController(), appCoordinator: self)
        
        childCoordinators = [listCoordinator]
        listCoordinator.start()
        assignRootViewController(navController: listCoordinator.navigationController)
    }
}

extension AppCoordinator {
    private func assignRootViewController(navController: UINavigationController) {
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }
}
