//
//  ListViewController.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit

enum BBSeason: Int, CaseIterable {
    case all = 0
    case one
    case two
    case three
    case four
    case five
    
    var scopeTitle: String {
        switch self {
        case .all:
            return "All"
        default:
            return "\(self)"
        }
    }
}

class ListViewController: UIViewController {
        
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.rowHeight = 90
            tableView.backgroundColor = .white
            tableView.tableFooterView = UIView()
                        
            tableView.register(ListTableViewCell.self, forCellReuseIdentifier: ListTableViewCell.reuseIdentifier)
        }
    }
    
    var coordinator: ListCoordinator!
    var dataManager: ListDataManager!
    
    var searchController = UISearchController()
    var searchScopeDatasource = BBSeason.allCases.map({$0.scopeTitle})
    var selectedSeason: BBSeason = .all {
        didSet {
            dataSourceProvider.performSearch(searchQuery: selectedSearchText, seasonFilter: selectedSeason)
        }
    }
    
    var selectedSearchText: String? {
        didSet {
            dataSourceProvider.performSearch(searchQuery: selectedSearchText, seasonFilter: selectedSeason)
        }
    }
    
    private lazy var dataSourceProvider: ListDataSourceProvider = ListDataSourceProvider(dataManager: dataManager, tableView: tableView, delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Strings.Titles.ListViewControllerTitle
        initialConfigureUI()
        
        dataSourceProvider.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavBar()
    }
    
    fileprivate func configureNavBar() {
        navigationItem.largeTitleDisplayMode = .always
    }
    
    func initialConfigureUI() {
        edgesForExtendedLayout = [.top, .bottom]
        
        configureSearchController()
    }
    
    func configureSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = Strings.Misc.searchCharactersPlaceholder
        searchController.searchBar.delegate = self
        
        searchController.searchBar.scopeButtonTitles = searchScopeDatasource
        
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
    }
}

extension ListViewController: ListDataSourceProviderDelegate {
    func characterSelected(_ character: BBCharacter) {
        pushCharacterDetail(detail: character)
    }
}

extension ListViewController {
    private func pushCharacterDetail(detail: BBCharacter) {
        coordinator.pushCharacterDetail(detail)
    }
}

extension ListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("New scope index is now \(selectedScope)")
        
        guard let season = BBSeason(rawValue: selectedScope) else { return }
        selectedSeason = season
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        selectedSearchText = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.isActive = false
        dataSourceProvider.performSearch(searchQuery: nil, seasonFilter: .all)
    }
}
