//
//  DetailViewController.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.estimatedRowHeight = 90
            tableView.rowHeight = UITableView.automaticDimension
            tableView.tableFooterView = UIView()

            tableView.register(DetailImageTableViewCell.self, forCellReuseIdentifier: DetailImageTableViewCell.reuseIdentifier)
            tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: DetailTableViewCell.reuseIdentifier)
        }
    }
    
    var coordinator: ListCoordinator!
    var selectedCharacter: BBCharacter!
    
    private lazy var dataSourceProvider: DetailDatasourceProvider = DetailDatasourceProvider(selectedCharacter: selectedCharacter, tableView: tableView)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedCharacter.name
        adjustLargeTitleSize()
        
        initialConfigureUI()
        
        dataSourceProvider.populateDatasource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavBar()
    }
    
    fileprivate func configureNavBar() {
        navigationItem.largeTitleDisplayMode = .always
    }
    
    func initialConfigureUI() {
        edgesForExtendedLayout = [.top, .bottom]
    }
}
