//
//  BreakingBadAPIServiceMock.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

@testable import BreakingBadTest

protocol BreakingBadAPIServiceMockType: BreakingBadAPIServiceType {
    var receivedData: Data! { get set }
}

struct BreakingBadAPIServiceMock: BreakingBadAPIServiceMockType {
    var receivedData: Data!
    
    var networkManager: NetworkManager
    
    func fetchCharacters(completionHandler: @escaping FetchCharactersCompletionHandler) {        
        parseCharacterData(receivedData, completionHandler: completionHandler)
    }
}
