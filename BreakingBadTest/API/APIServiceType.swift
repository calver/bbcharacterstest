//
//  APIServiceType.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

protocol APIService {
    var networkManager: NetworkManager { get set }
}

typealias CharacterResult = Result<[BBCharacter], Error>
typealias FetchCharactersCompletionHandler = ((CharacterResult) -> ())
protocol CharactersAPIServiceType: APIService {
    func fetchCharacters(completionHandler: @escaping FetchCharactersCompletionHandler)
}
