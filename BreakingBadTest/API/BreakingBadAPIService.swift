//
//  APIService.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

protocol BreakingBadAPIServiceType: CharactersAPIServiceType {
    func parseCharacterData(_ data: Data, completionHandler: @escaping FetchCharactersCompletionHandler)
}

extension BreakingBadAPIServiceType {
    func parseCharacterData(_ data: Data, completionHandler: @escaping FetchCharactersCompletionHandler) {
        return completionHandler(BreakingBadAPIService.performParse(data))
    }
}

struct BreakingBadAPIService: BreakingBadAPIServiceType {
    
    var networkManager: NetworkManager
    
    func fetchCharacters(completionHandler: @escaping FetchCharactersCompletionHandler) {
        let urlString = Strings.Network.charactersEndpointUrl
        
        guard let url = URL(string: urlString) else {
            completionHandler(.failure(FetchError.invalidURL))
            return
        }
        
        networkManager.loadData(from: url) { result in
            switch result {
            case .failure(_):()
            case .success(let data):
                self.parseCharacterData(data, completionHandler: completionHandler)
            }
        }
    }
}

extension BreakingBadAPIService {
    static func performParse(_ data: Data) -> CharacterResult {
        do {
            let characters = try JSONDecoder().decode([BBCharacter].self, from: data)
            return CharacterResult.success(characters)
        }
        catch{
            print(error)
            return CharacterResult.failure(FetchError.invalidData)
        }
    }
}
