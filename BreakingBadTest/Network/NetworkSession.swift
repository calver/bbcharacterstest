//
//  NetworkSession.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

protocol NetworkSession {
    func loadData(from url: URL, completionHandler: @escaping NetworkResponse)
    func postData(withRequest urlRequest: URLRequest, completionHandler: @escaping NetworkResponse)
}
