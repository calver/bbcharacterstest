//
//  NetworkManager.swift
//  BreakingBadTest
//
//  Created by paul calver on 03/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

enum FetchError: Error, Equatable {
    case invalidURL
    case invalidData
    case retrievalError
    
    static func == (lhs: FetchError, rhs: FetchError) -> Bool {
        switch (lhs, rhs) {
        case (.invalidURL, .invalidURL): return true
        case (.invalidData, .invalidData): return true
        case (.retrievalError, .retrievalError): return true
        default: return false
        }
    }
}

extension FetchError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString("Description of invalid URL", comment: "Invalid URL")
        case .invalidData:
            return NSLocalizedString("Description of invalid data", comment: "Invalid Data")
        case .retrievalError:
            return NSLocalizedString("Description of retrieval error", comment: "Retrieval Error")
        }
    }
}

typealias HTTPStatusCode = Int

enum NetworkResult: Equatable {
    case success(Data)
    case failure(Error?)
    
    static func == (lhs: NetworkResult, rhs: NetworkResult) -> Bool {
        switch (lhs, rhs) {
        case let (.success(lhsData), .success(rhsData)): return lhsData == rhsData
        case let (.failure(lhsError), .failure(rhsError)): return lhsError?.localizedDescription == rhsError?.localizedDescription
        case (.success, _), (.failure, _): return false
        }
    }
}

class NetworkManager: NSObject {
    let session: NetworkSession
    
    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }
    
    func loadData(from url: URL, completionHandler: @escaping (NetworkResult) -> Void) {
        session.loadData(from: url) { [weak self] data, response, error in
            guard let self = self else { return }
            self.handleResponse(completionHandler: completionHandler, data: data, response: response, error: error)
        }
    }
    
    func postData(withRequest urlRequest: URLRequest, completionHandler: @escaping (NetworkResult) -> Void) {
        session.postData(withRequest: urlRequest) { [weak self] data, response, error in
            guard let self = self else { return }
            self.handleResponse(completionHandler: completionHandler, data: data, response: response, error: error)
        }
    }
    
    func statusCodeFromResponse(_ response: URLResponse?) -> Int? {
        guard let response = response, let statusCode = (response as? HTTPURLResponse)?.statusCode else {
            return nil
        }
        
        return statusCode
    }
}

extension NetworkManager {
    fileprivate func handleResponse(completionHandler: (NetworkResult) -> Void, data: Data?, response: URLResponse?, error: Error?) {
        
        var result: NetworkResult = .failure(FetchError.retrievalError)
        
        // error ?
        if let error = error {
            result = .failure(error)
            completionHandler(result)
            return
        }
        

        // handle returned data, or error if none
        if let data = data {
            result = NetworkResult.success(data)
        } else {
            result = error.map(NetworkResult.failure) ?? .failure(FetchError.retrievalError)
        }
        
        completionHandler(result)
    }
}
