//
//  URLSessionMock.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

class URLSessionMock: URLSession {
    
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    
    var data: Data?
    var error: Error?
    var urlResponse: URLResponse?
    
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.error = error
        self.urlResponse = urlResponse
    }
    
    override func dataTask(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        let urlResponse = self.urlResponse
        
        return URLSessionDataTaskMock { completionHandler(data, urlResponse, error) }
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        let urlResponse = self.urlResponse
        
        return URLSessionDataTaskMock { completionHandler(data, urlResponse, error) }
    }
}
