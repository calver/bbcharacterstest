//
//  TestNetworkManager.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation
@testable import BreakingBadTest

class TestNetworkManager: NetworkManager {
    
    var invokedStatusCode:Int!
    
    override func statusCodeFromResponse(_ response: URLResponse?) -> Int? {
        return invokedStatusCode
    }
}
