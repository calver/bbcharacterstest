//
//  URLSessionDataTaskMock.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import Foundation

class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void
    
    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    override func resume() {
        closure()
    }
}

