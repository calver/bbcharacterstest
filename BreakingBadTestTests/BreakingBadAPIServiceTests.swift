//
//  APIServiceTests.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import XCTest
@testable import BreakingBadTest

class APIServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSuccessfulFetchAndParseOfCharacters() {
        // given
        var data: Data?
        
        do {
            let url = Bundle.main.url(forResource: "TestCharactersJsonResponse", withExtension: "json")!
            data = try Data(contentsOf: url)
        } catch {
            print(error.localizedDescription)
        }
        
        let session = URLSessionMock(data: data, urlResponse: nil, error: nil)
        let manager = NetworkManager(session: session)
        
        session.data = data
        
        var apiService = BreakingBadAPIServiceMock(networkManager: manager)
        apiService.receivedData = data
        
        var receivedCharacters = [BBCharacter]()
        
        let exp = expectation(description: "characters received")
        
        let handler: FetchCharactersCompletionHandler = { result in
            switch result {
            case .success(let characters):
                receivedCharacters = characters
            case .failure(let error):
                print(error.localizedDescription)
            }
            
            exp.fulfill()
        }
        
        //when
        apiService.fetchCharacters(completionHandler: handler)
        waitForExpectations(timeout: 3)
        
        // then
        XCTAssertEqual(receivedCharacters.count, 63, "We should have fetched exactly 63 BB characters.")
    }
    
    func testInvalidDataReceived() {
        // given
        let data = Data([0, 1, 0, 1])
        
        let session = URLSessionMock(data: data, urlResponse: nil, error: nil)
        let manager = NetworkManager(session: session)
        
        session.data = data
        
        var apiService = BreakingBadAPIServiceMock(networkManager: manager)
        // unexpected data received
        apiService.receivedData = data
        
        var receivedCharacters = [BBCharacter]()
        var receivedError: Error?
        
        let exp = expectation(description: "characters received")
        
        let handler: FetchCharactersCompletionHandler = { result in
            switch result {
            case .success(let characters):
                receivedCharacters = characters
            case .failure(let error):
                receivedError = error
            }
            
            exp.fulfill()
        }
        
        //when
        apiService.fetchCharacters(completionHandler: handler)
        waitForExpectations(timeout: 3)
        
        // then
        XCTAssertEqual(receivedCharacters.count, 0, "We should have 0 BB characters.")
        XCTAssertEqual(receivedError!.localizedDescription, FetchError.invalidData.localizedDescription, "We should have received FetchError.invalidData.")
    }
}
