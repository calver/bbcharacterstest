//
//  ListDatasourceProviderTests.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import XCTest
@testable import BreakingBadTest

class ListDatasourceProviderTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testSearchForWalter() {
        // given
        var data: Data?
        
        do {
            let url = Bundle.main.url(forResource: "TestCharactersJsonResponse", withExtension: "json")!
            data = try Data(contentsOf: url)
        } catch {
            print(error.localizedDescription)
        }
        
        let result = BreakingBadAPIService.performParse(data!)
        
        guard case let CharacterResult.success(receivedCharacters) = result else {
            XCTFail("data parsing failed")
            return
        }
        
        XCTAssertEqual(receivedCharacters.count, 63, "We should have fetched exactly 63 BB characters.")
        
        let searchText = "walter"
        
        let searchResults = ListDatasourceProviderMock.performSearch(onCharacters: receivedCharacters, searchQuery: searchText, seasonFilter: .all)
        
        XCTAssertEqual(searchResults.count, 2, "We should have found exactly 2 walter's.")
    }
}
