//
//  NetworkManagerTests.swift
//  BreakingBadTestTests
//
//  Created by paul calver on 04/05/2020.
//  Copyright © 2020 qalvapps. All rights reserved.
//

import XCTest
@testable import BreakingBadTest

class NetworkManagerTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSuccessfulResponse() {
        // given
        let data = Data([0, 1, 0, 1])
        
        let session = URLSessionMock(data: data, urlResponse: nil, error: nil)
        let manager = NetworkManager(session: session)
        
        session.data = data
        
        //then
        let url = URL(fileURLWithPath: "url")
        var result: NetworkResult?
        manager.loadData(from: url) { result = $0 }
        
        // then
        XCTAssertEqual(result!, .success(data))
    }
}
